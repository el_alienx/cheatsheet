// Core
import React from "react";

// Internal
import "./styles/style.sass";

export default function App() {
  return (
    <div className="App">
      <p>
        Edit <code>src/App.js</code> and save to reload.
      </p>
    </div>
  );
}
